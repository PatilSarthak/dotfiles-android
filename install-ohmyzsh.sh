

pkg update && pkg upgrade

#dependencies
pkg install git zsh 

#installing zsh
cp ~/.zshrc ~/
git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh
chsh -s zsh


#cloning some awesome plugins 
cd ~/.oh-my-zsh/plugins/

git clone https://github.com/zsh-users/zsh-autosuggestions.git 

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git
